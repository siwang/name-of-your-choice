#include <utility>

//
// Created by siwang@INTRANET.EPFL.CH on 22.10.20.
//

#ifndef SERIES_SERIES_HH
#define SERIES_SERIES_HH
class Series{
public:
    std::basic_string<char> getmethod(){return method;};
    void setmethod(std::string input);
    virtual double compute(unsigned int N) = 0;
private:
    std::string method;
};
void Series::setmethod(std::string input) {
    this->method = input;
}
class ComputeArithmetic : public Series{
public:
    double compute(unsigned int N) override {return (N*(N+1)/2);};
private:
    std::string method;
};
class ComputePi : public Series{
public:
    double compute(unsigned int N) override {
        float result = 0;
        for(float i=1;i<=N;i=i+1){
            result += pow((1/i),2);
        }
        return (sqrt(result*6));
    }
private:
    std::string method;
};
#endif //SERIES_SERIES_HH
