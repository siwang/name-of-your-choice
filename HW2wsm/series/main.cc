#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include "Series.hh"

/* -------------------------------------------------------------------------- */
using namespace std;

int main(){
    unsigned int N;
    cout << "Please enter the value of N: \n";
    cin >> N;
    string method;
    cout << "Please enter calculation method: \n";
    cin >> method;
    if(method == "ComputeArithmetic"){
        ComputeArithmetic result;
        result.setmethod(method);
        cout << "The method used is: "<<result.getmethod()<<endl;
        cout << "The result is: "<<result.compute(N)<<"\n";;
    }
    else{
        if(method == "ComputePi"){
            ComputePi result;
            result.setmethod(method);
            cout << "The method used is: "<<result.getmethod()<<endl;
            cout << "The result is: "<<result.compute(N)<<"\n";;
        }
        else{
            cout << "Wrong Method \n";
        }
    }
    return 0;
}



