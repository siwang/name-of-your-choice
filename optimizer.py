#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 13:40:06 2020

@author: Simiao Wang & Haoran Shi
"""
#First import all the necessary libarary

import scipy.optimize
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()
axe = Axes3D(fig)

#define the calculation function S(x)
def S(x):
    l =((4,0),(1,3))
    v1 = np.array(l)
    t = ((0),(1))
    v2 = np.array(t)
    Sx = x.T@v1@x-x.T@v2
    return Sx

#define the callback function

x = []
y = []
W = []

def callbackF(xk):
   global x
   global y
   global W
   fill1=[0]
   fill2=[0]
   fill3=[0]
   fill1[0]=xk[0]
   fill2[0]=xk[1]
   fill3[0]=S(xk)
   x = x+fill1
   y = y+fill2
   W = W+fill3


#define the function for question 1.1, with function and initial vector as an input to find the minimum value
def minimum(fun,x):
    res = scipy.optimize.minimize(fun,x,method = 'BFGS',callback=callbackF)
    return res

#define the initial vector as [3 3]
x0 = ([3],[3])
x0 = np.array(x0)
a = minimum(S,x0)


#define the range of X and Y values
X = np.arange(-3, 3, 0.01)
Y = np.arange(-3, 3, 0.01)
Z = np.zeros((600,600))

#calculate Y values based on X and Y
for i in range(600):
    for j in range(600):
        M = ([X[i]],[Y[j]])
        M = np.array(M)
        Z[i,j]=S(M)
X, Y = np.meshgrid(X, Y)
#plot the surface and iterations       
axe.plot_surface(X,Y,Z,alpha = 0.5)
axe.plot(x,y,W,'o--')
axe.set_title("BFGS")