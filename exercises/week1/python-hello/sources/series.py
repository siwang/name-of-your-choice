#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 16:43:41 2020

@author: siwang
"""

def computeSeries(Niterations):
    sn = 0
    for i in range(0,Niterations+1):
        sn = sn + i
    return(sn)
