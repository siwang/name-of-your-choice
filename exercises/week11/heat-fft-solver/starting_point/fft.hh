#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>

/* ------------------------------------------------------ */

struct FFT {

  static Matrix<complex> transform(Matrix<complex>& m);
  static Matrix<complex> itransform(Matrix<complex>& m);


  static Matrix<std::complex<int>> computeFrequencies(int size);
};

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) {
  uint size = m_in.size();
  Matrix<complex> m_out(size);
  fftw_complex *out=(fftw_complex*) m_out.data();
  fftw_complex *in = (fftw_complex*) m_in.data();
  fftw_plan p;
  p = fftw_plan_dft_2d(size,size, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(p);
  fftw_destroy_plan(p);
  return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
  uint size = m_in.size();
  Matrix<complex> m_out(size);
  fftw_complex *out=(fftw_complex*) m_out.data();
  fftw_plan p;
  fftw_complex *in = (fftw_complex*) m_in.data();
  p = fftw_plan_dft_2d(size,size, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(p);
  fftw_destroy_plan(p);
  m_out/=double(size*size);
  return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<std::complex<int>> FFT::computeFrequencies(int size) {
  Matrix<std::complex<int>> freq(size);

  for (auto&& entry : index(freq)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    if (i< size/2+size%2){val.real(i);}
    else{val.real(-(size-i));}

    if (j< size/2+size%2){val.imag(j);}
    else{val.imag(-(size-j));}
  }
  return freq;
}

#endif  // FFT_HH
