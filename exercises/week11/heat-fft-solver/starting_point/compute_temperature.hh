#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"
#include "matrix.hh"

//! Compute contact interaction between ping-pong balls
class ComputeTemperature : public Compute {

  // Virtual implementation
public:
  void setdT(Real dT);
  void setHeatConductivity(Real HeatConductivity);
  void setDensity(Real Density);
  void setHeatCapacity(Real HeatCapacity);
  //! Penalty contact implementation
  void compute(System& system) override;


private:
  Real dT;
  Real HeatConductivity;
  Real Density;
  Real HeatCapacity;
};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
