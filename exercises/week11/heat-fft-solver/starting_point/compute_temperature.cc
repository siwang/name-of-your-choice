#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>

/* -------------------------------------------------------------------------- */

void ComputeTemperature::compute(System& system) {
  UInt size = std::sqrt(system.getNbParticles());

  MaterialPoint &p0 = static_cast<MaterialPoint &>(system.getParticle(0));
  MaterialPoint &px = static_cast<MaterialPoint &>(system.getParticle(size-1));
  MaterialPoint &py = static_cast<MaterialPoint &>(system.getParticle(size*(size-1)));

  Real Lx = std::abs(px.getPosition()[0]-p0.getPosition()[0]);
  Real Ly = std::abs(py.getPosition()[1]-p0.getPosition()[1]);

  Matrix<complex> Temp(size);
  Matrix<complex> dTemp(size);
  Matrix<complex> Heatsource(size);

  Matrix<complex> TempF(size);
  Matrix<complex> dTempF(size);
  Matrix<complex> HeatsourceF(size);

  Matrix<std::complex<int>> freq = FFT::computeFrequencies(size);

  for (auto&& entry : index(Temp)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    MaterialPoint& point =
        static_cast<MaterialPoint&>(system.getParticle(size * j + i));
    Temp(i, j) = point.getTemperature();
    Heatsource(i, j) = point.getHeatRate();
  }

  TempF = FFT::transform(Temp);
  HeatsourceF = FFT::transform(Heatsource);

  for (auto&& entry : index(TempF)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    Real qx = (2*M_PI)/Lx*freq(i,j).real()/size;
    Real qy = (2*M_PI)/Ly*freq(i,j).imag()/size;

    dTempF(i,j)=(1/(Density*HeatCapacity))*(HeatsourceF(i,j)-HeatConductivity*TempF(i,j)*(pow(qx,2)+pow(qy,2)));
  }
  dTemp = FFT::itransform(dTempF);

  for (auto&& entry : index(Temp)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    MaterialPoint& point =
        static_cast<MaterialPoint&>(system.getParticle(size * j + i));
    point.getTemperature() = point.getTemperature() + dT * dTemp(i, j).real();
  }

}


void ComputeTemperature::setDensity(Real Density) {
  this -> Density = Density;
}

void ComputeTemperature::setHeatCapacity(Real HeatCapacity) {
  this -> HeatCapacity = HeatCapacity;
}

void ComputeTemperature::setdT(Real dT) {
  this -> dT = dT;
}

void ComputeTemperature::setHeatConductivity(Real HeatConductivity) {
  this -> HeatConductivity = HeatConductivity;
}

/* -------------------------------------------------------------------------- */
