#include "fft.hh"
#include "my_types.hh"
#include <gtest/gtest.h>

/*****************************************************************/
TEST(FFT, transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
  }

  Matrix<complex> res = FFT::transform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (std::abs(val) > 1e-10)
      std::cout << i << "," << j << " = " << val << std::endl;

    if (i == 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else if (i == N - 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
  }
}
/*****************************************************************/

TEST(FFT, inverse_transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
  }

  Matrix<complex> med = FFT::transform(m);
  Matrix<complex> res = FFT::itransform(med);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    ASSERT_NEAR(std::abs(val), std::abs(m(i,j)), 1e-10);
  }
}
/*****************************************************************/
TEST(FFT, Compute_frequency) {
  int N = 20;
  Matrix<std::complex<int>> freq(N);
  freq = FFT::computeFrequencies(N);

  for (auto&& entry : index(freq)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (i< N/2+N%2){
      ASSERT_NEAR(val.real(), i, 1e-10);
    }
    else{
      ASSERT_NEAR(val.real(), (i-N), 1e-10);
    }

    if (j< N/2+N%2){
      ASSERT_NEAR(val.imag(), j, 1e-10);
    }
    else{
      ASSERT_NEAR(val.imag(), (j-N), 1e-10);
    }
  }
  std::cout << "To compare the frequency computed by python script of size " << N<< std::endl;
  std::cout << "For real parts:"<<std::endl;
  for (auto&& entry : index(freq)) {
    int i = std::get<0>(entry);
    auto& val = std::get<2>(entry);
    std::cout << val.real()<<" ";
    if (i==N-1){
      std::cout<<std::endl;
    }
  }
  std::cout << "For imaginery parts:"<<std::endl;
  for (auto&& entry : index(freq)) {
    int i = std::get<0>(entry);
    auto& val = std::get<2>(entry);
    std::cout << val.imag()<<" ";
    if (i==N-1){
      std::cout<<std::endl;
    }
  }
}

/*****************************************************************/