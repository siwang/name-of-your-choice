#include "fft.hh"
#include "my_types.hh"
#include <gtest/gtest.h>
#include "compute_temperature.hh"
#include "material_point.hh"
#include "material_points_factory.hh"

/*****************************************************************/
TEST(ComputeTemperature, ex4_2) {
  Real Temp = 25;
  Real hv = 0;
  Real Density = 1;
  Real HeatConductivity = 2;
  Real HeatCapacity = 3;
  Real dT = 1e-4;
  UInt size = 20;
  Real Lx=2.;
  Real Ly=2.;

  MaterialPointsFactory::getInstance();
  std::vector<MaterialPoint> point_list;
  System system;

  for (UInt j = 0; j < size; j++) {
    for (UInt i = 0; i < size; i++) {
      MaterialPoint point;
      point.getPosition()[0] = double(-Lx / 2. + (i+1) * Lx / (size + 1));
      point.getPosition()[1] = double(-Ly / 2. + (j+1) * Ly / (size + 1));
      point.getPosition()[2] = 0.;
      point.getTemperature() = Temp;
      point.getHeatRate() = hv;
      point_list.push_back(point);
      system.addParticle(std::make_shared<MaterialPoint>(point));
    }
  }

  ComputeTemperature m;
  UInt step = 1000;
  m.setHeatConductivity(HeatConductivity);
  m.setHeatCapacity(HeatCapacity);
  m.setdT(dT);
  m.setDensity(Density);

  for (int i = 0; i < step; ++i) {
    m.compute(system);
  }



  for (UInt j = 0; j < size; j++) {
    for (UInt i = 0; i < size; i++) {
      MaterialPoint &point = static_cast<MaterialPoint &>(system.getParticle(size*j+i));
      ASSERT_NEAR(point.getTemperature(),Temp,1e-6);
    }
  }
}
/*****************************************************************/
TEST(ComputeTemperature, ex4_3) {
  Real Density = 1;
  Real HeatConductivity = 1;
  Real HeatCapacity = 1;
  Real dT = 1e-6;
  UInt size = 20;
  Real Lx=2.;
  Real Ly=2.;
  Real L =2-2*Lx/(size+1);
  UInt step = 800;

  MaterialPointsFactory::getInstance();
  std::vector<MaterialPoint> point_list;
  System system;

  for (UInt j = 0; j < size; j++) {
    for (UInt i = 0; i < size; i++) {
      MaterialPoint point;
      point.getPosition()[0] = double(-Lx / 2. + (i+1) * Lx / (size + 1));
      Real x = point.getPosition()[0];
      point.getPosition()[1] = double(-Ly / 2. + (j+1) * Ly / (size + 1));
      Real y = point.getPosition()[1];
      point.getPosition()[2] = 0.;
      point.getTemperature() = sin(2*M_PI*x/L);
      point.getHeatRate() = pow((2*M_PI/L),2)*sin(2*M_PI*x/L);
      point_list.push_back(point);
      system.addParticle(std::make_shared<MaterialPoint>(point));
    }
  }

  ComputeTemperature m;
  m.setHeatConductivity(HeatConductivity);
  m.setHeatCapacity(HeatCapacity);
  m.setdT(dT);
  m.setDensity(Density);

  for (int i = 0; i < step; ++i) {
    m.compute(system);
  }

  for (UInt j = 0; j < size; j++) {
    for (UInt i = 0; i < size; i++) {
      MaterialPoint &point = static_cast<MaterialPoint &>(system.getParticle(size*j+i));
      Real x = point.getPosition()[0];
      ASSERT_NEAR(point.getTemperature(),sin(2*M_PI*x/L),1e-2);
    }
  }
}

/*****************************************************************/
TEST(ComputeTemperature, ex4_4) {
  Real Density = 1;
  Real HeatConductivity = 1;
  Real HeatCapacity = 1;
  Real dT = 1e-6;
  UInt size = 20;
  Real Lx = 2.;
  Real Ly = 2.;
  Real L = 2 - 2 * Lx / (size + 1);
  UInt step = 1000;

  MaterialPointsFactory::getInstance();
  std::vector<MaterialPoint> point_list;
  System system;

  for (UInt j = 0; j < size; j++) {
    for (UInt i = 0; i < size; i++) {
      MaterialPoint point;
      point.getPosition()[0] = double(-Lx / 2. + (i + 1) * Lx / (size + 1));
      Real x = point.getPosition()[0];
      point.getPosition()[1] = double(-Ly / 2. + (j + 1) * Ly / (size + 1));
      Real y = point.getPosition()[1];
      point.getPosition()[2] = 0.;
      if (x==0.5){
        point.getHeatRate() = 1;
      }
      else{
        if (x==(-0.5)){
          point.getHeatRate() = -1;
        }
        else{
          point.getHeatRate() = 0;
        }
      }
      if(x<=-0.5){
        point.getTemperature() = -x-1;
      }
      else{
        if(x<=0.5){
          point.getTemperature() = x;
        }
        else{
          point.getTemperature() = -x+1;
        }
      }
      point_list.push_back(point);
      system.addParticle(std::make_shared<MaterialPoint>(point));
    }
  }
  ComputeTemperature m;
  m.setHeatConductivity(HeatConductivity);
  m.setHeatCapacity(HeatCapacity);
  m.setdT(dT);
  m.setDensity(Density);

  for (int i = 0; i < step; ++i) {
    m.compute(system);
  }
  for (UInt j = 0; j < size; j++) {
    for (UInt i = 0; i < size; i++) {
      MaterialPoint &point = static_cast<MaterialPoint &>(system.getParticle(size*j+i));
      Real x = point.getPosition()[0];
      if(x<=-0.5){
        ASSERT_NEAR(point.getTemperature(),-x-1,1e-2);
      }
      if(x>0.5){
        ASSERT_NEAR(point.getTemperature(),-x+1,1e-2);
      }
      if(-0.5<x&&x<=0.5){
        ASSERT_NEAR(point.getTemperature(),x,1e-2);
      }
    }
  }
}
