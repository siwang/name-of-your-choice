#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include "Series.hh"

/* -------------------------------------------------------------------------- */
using namespace std;

int main(){
    ComputeArithmetic a;
    cout << a.compute(10)<<"\n";
    ComputePi b;
    cout << b.compute(10)<<"\n";
    return 0;
}



