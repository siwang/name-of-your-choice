#ifndef __COMPUTE_ALGEBRAIC_HH__
#define __COMPUTE_ALGEBRAIC_HH__
/* -------------------------------------------------------------------------- */
#include "series.hh"
/* -------------------------------------------------------------------------- */

class ComputeArithmetic : public Series {
public:
  ComputeArithmetic();
  double getAnalyticPrediction() override;
};

#endif /* __COMPUTE_ALGEBRAIC_HH__ */
