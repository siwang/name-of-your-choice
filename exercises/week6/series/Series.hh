//
// Created by siwang@INTRANET.EPFL.CH on 22.10.20.
//

#ifndef SERIES_SERIES_HH
#define SERIES_SERIES_HH
class Series{
public:
    virtual double compute(unsigned int N) = 0;
};
class ComputeArithmetic : public Series{
public:
    double compute(unsigned int N) override {return (N*(N+1)/2);};
};
class ComputePi : public Series{
public:
    double compute(unsigned int N) override {
        float result = 0;
        for(float i=1;i<=N;i=i+1){
            result += pow((1/i),2);
        }
        return (sqrt(result*6));
    }
};
#endif //SERIES_SERIES_HH
