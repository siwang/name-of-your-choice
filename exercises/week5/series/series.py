#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Spyder Editor

Auther: Simiao Wang
"""
import numpy as np
import math
import argparse
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()

class Series:
    def __init__(self):
        self.current_term = 0
        self.current_value = 0
    def compute(self, N):
        raise Exception("pure virtual function")
    def getAnalyticPrediction(self):
        raise Exception("pure virtual function")

class ComputeArithmetic(Series):
    def __init__(self,N):
        self.N = N
    def compute(self,N):
        series_value = N*(N+1)/2
        return series_value
    def getAnalyticPrediction(self):
        prediction = self.N*(self.N+1)/2
        return prediction

class ComputePi(Series):
    def __init__(self,N):
        self.N = N
    def compute(self,N):
        res = 0
        N = int(N)
        for i in range (N):
            res = res + 1/(i+1)**2
        series_value = np.sqrt(6*res)
        return series_value
    def getAnalyticPrediction(self):
        prediction = math.pi
        return prediction
        
class DumperSeries:
    def __init__(self, Series):
        self.series = Series
    def dump(self):
        raise Exception("pure virtual function")

class PrintSeries(DumperSeries):
    def __init__(self, frequency, maxiter,Series):
        self.frequency = frequency
        self.maxiter = maxiter
        self.series = Series
    def dump(self):
        res = []
        iteration = 1
        while iteration < self.maxiter:
            res.append(self.series.compute(iteration))
            iteration += self.frequency
        return res

class PlotSeries(DumperSeries):
    def __init__(self, frequency, maxiter,Series):
        self.frequency = frequency
        self.maxiter = maxiter
        self.series = Series
    def dump(self):
        res = []
        it = []
        iteration = 1
        while iteration < self.maxiter:
            res.append(self.series.compute(iteration))
            it.append(iteration)
            iteration += self.frequency
        length = len(it)
        lim = [self.series.getAnalyticPrediction()]*length
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        cal = ax.plot(it,res,"-",c='green')
        limit = ax.plot(it,lim,"--",c='red')
        plt.xlabel('iteration')
        plt.ylabel('Value')
        plt.show()
        

parser.add_argument('--N', type=float,default = 1,
                    help='Value of N')
parser.add_argument('--method', type=str,
                    help='Calculation method used')
parser.add_argument('--frequency', type=float,default = 1,
                    help='Value of frequency')
parser.add_argument('--option', type=str,
                    help='Print or Plot')

args = parser.parse_args()

if args.method == "ComputeArithmetic":
    a = ComputeArithmetic(args.N)
    print(a.compute(args.N))
elif args.method == "ComputePi":
    a = ComputePi(args.N)
    print(a.compute(args.N))
else:
    print("Please enter correct method.")

if args.frequency > args.N:
    print("Frequency value is larger than maxiter")
else:
    if args.option == "Print":
        b = PrintSeries(args.frequency,args.N,a)
        print(b.dump())
    elif args.option == "Plot":
        PlotSeries(args.frequency,args.N,a).dump()
    else:
        print("Please enter the correct option")



    
    
    
