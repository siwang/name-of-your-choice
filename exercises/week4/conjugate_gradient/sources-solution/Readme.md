# Instructions to launch the program

In order to launch the program, simply launch:

python3 ./main.py a_11 a_12 a_21 a_22 b_1 b_2

With the coefficients a_ij and b_i the terms of the matrix A and vector b in the quadratic form 1/2 x^T A x - x^T b, as advertise by the help:

python3 ./main.py -h

This will compute the minimum of the quadratic function using one optimizer.

In order to change the method you can add an additional argument:

python3 ./main.py a_11 a_12 a_21 a_22 b_1 b_2 -m cg
python3 ./main.py a_11 a_12 a_21 a_22 b_1 b_2 -m BFGS

In order to make a plot, use the following option:

python3 ./main.py a_11 a_12 a_21 a_22 b_1 b_2 -p 1