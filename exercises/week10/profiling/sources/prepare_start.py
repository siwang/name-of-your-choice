#!/usr/bin/env python3
import argparse
import subprocess
import functools
import sys
import re
import os


from contextlib import contextmanager

tags = {'begin': 'EXERCISE_BEGIN_CORRECTION',
        'end': 'EXERCISE_END_CORRECTION'}

export_regexps = [
    r".*\.cc$",
    r".*\.hh$",
    "CMakeLists.txt"
]

complete_regex = functools.reduce(lambda x, y: x + "|" + y,
                                  export_regexps)
pattern = re.compile(complete_regex)


def strip_correction(filename, outfilename):
    print(filename)

    sed_args = ['sed', '-e', r'/^\s*{begin}/,/{end}/d'.format(**tags), filename]

    with open(outfilename, 'wb') as out:
        subprocess.run(sed_args, stdout=out)


def export_directory(dirname, outdirname):
    file_filter = lambda f: os.path.isfile(f)  # noqa
    extension_filter = lambda f: pattern.match(f)  # noqa
    objects = (f for f in os.listdir(dirname)
               if file_filter(f) and extension_filter(f))

    for filename in objects:
        strip_correction(filename, os.path.join(outdirname, filename))


@contextmanager
def git_tag_manager(tag):
    """Git tag context manager"""
    out = {'stderr': subprocess.DEVNULL,
           'stdout': subprocess.DEVNULL}
    try:
        subprocess.run(['git', 'checkout', tag], check=True, **out)
    except subprocess.CalledProcessError:
        print('Check that your working git repository is clean')
        sys.exit(1)

    yield
    subprocess.run(['git', 'checkout', 'master'], **out)


parser = argparse.ArgumentParser(description="Export starting point")
parser.add_argument("source", help="Source directory")
parser.add_argument("destination", help="Destination directory")
parser.add_argument('-t', '--tag', help="Git tag to export", default='master')
args = parser.parse_args()

destination_exists = os.path.exists(args.destination)
destination_isdir = os.path.isdir(args.destination)

if destination_exists and not destination_isdir:
    print("{}: {} exists but is not a directory", sys.argv[0], args.destination)
    sys.exit(1)
elif not destination_exists:
    os.mkdir(args.destination)

with git_tag_manager(args.tag):
    export_directory(args.source, args.destination)
