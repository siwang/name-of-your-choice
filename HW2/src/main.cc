#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <math.h>
#include "Series.hh"
#include "DumperSeries.hh"

/* -------------------------------------------------------------------------- */
using namespace std;

int main(int argc, char **argv){
    ostream & os = cout;
    string method;unsigned int freq; unsigned int maxiter; string sp;string option;string ans;unsigned int precision;
    //cout <<"Please answer the following questions: \n"<<"Calculation method \n"<<"Print or Write \n"<<"Frequency \n"<<"Maxiter \n"<<"Need to change file format or not \n"<<"Separator \n";
    stringstream sstr;
    for(int i=1;i<argc;++i){
        sstr << argv[i]<<" ";
    }
    sstr >>method>>option>>freq>>maxiter>>precision>>ans>>sp;
    //cout << "Please enter calculation method: \n";
    //cin >> method;

    //cout << "Print or Write \n";
    //cin >> option;
    if(option == "Print"){
        //cout << "Please enter the value of frequency: \n";
        //cin >> freq;
        //cout << "Please enter the value of maxiter: \n";
        //cin >> maxiter;
        PrintSeries m;
        m.setmax(maxiter);
        m.setfreq(freq);
        m.setmethod(method);
        m.setPrecision(precision);
        m.dump(os);
    }
    else{
        if (option=="Write"){
            WriteSeries m;
            //cout << "Please enter the value of maxiter: \n";
            //cin >> maxiter;
            //cout << "Need to change file output from .txt form? (Y/N)  \n";

            //cin >> ans;
            if (ans == "Y"){
                //cout << "Please enter separator: \n";
                //cin >>sp;
                m.setSeparator(sp);
            }
            else{
                if (ans!="N"){
                    cout << "Wrong Answer";
                }
            }
            m.setmax(maxiter);
            m.setmethod(method);
            m.dump(os);
        }
        else{
            cout<<"Wrong option";
        }
    }

    return 0;
}



