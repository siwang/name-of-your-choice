#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <math.h>
#include "Series.hh"

std::basic_string<char> Series::getmethod() {
    return method;
};
void Series::setmethod(std::string input) {
    this->method = input;
};
double ComputeArithmetic::compute(unsigned int N){
    return (N*(N+1)/2);
};
double ComputeArithmetic::getAnalyticPrediction(unsigned int k) {
    return ((k*(k+1))/2);
};
double ComputePi::compute(unsigned int N) {
    double result = 0;
    for(double i=1;i<=N;i=i+1){
        result += pow((1/i),2);
    }
    return (sqrt(result*6));
};
double ComputePi::getAnalyticPrediction(unsigned int k) {
    return M_PI;
}

