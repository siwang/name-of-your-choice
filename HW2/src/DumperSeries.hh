//
// Created by siwang@INTRANET.EPFL.CH on 22.10.20.
//

#ifndef SERIES_DUMPERSERIES_HH
#define SERIES_DUMPERSERIES_HH

#include "Series.hh"
using namespace std;

class DumperSeries{
public:
    void setfreq(unsigned int input);
    void setmax(unsigned int input);
    void setmethod(std::string input);
    virtual void dump(ostream & os) = 0;
    virtual void setPrecision(unsigned int input)=0;
protected:
    //Series & series;
    unsigned int maxiter;
    unsigned int freq;
    unsigned int precision;
    string method;
};
class PrintSeries : public DumperSeries{
public:
    void dump(ostream &os) override;
    void setPrecision(unsigned int input)override;
protected:
    //ComputeArithmetic CA;
    //ComputePi CP;
private:
    string prt;
};
class WriteSeries : public DumperSeries{
public:
    basic_string<char> getdump(){return wrt;};
    void setSeparator(string input);
    void dump(ostream & os)override;
    void setPrecision(unsigned int input)override;
private:
    string wrt;
    string sp = " ";
};
inline std::ostream & operator <<(std::ostream & stream, DumperSeries & _this) {
    _this.dump(stream);
    return stream;
}
#endif //SERIES_DUMPERSERIES_HH
