#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include "DumperSeries.hh"
#include "Series.hh"

using namespace std;
void DumperSeries::setfreq(unsigned int input) {
    freq = input;
};
void DumperSeries::setmax(unsigned int input) {
    maxiter = input;
};
void DumperSeries::setmethod(std::string input) {
    method = input;
};

void PrintSeries::dump(ostream & os=cout){
    unsigned int c;
    unsigned int m = maxiter;
    unsigned int f = freq;
    unsigned int p = precision;
    c = floor(m/f);
    if(method == "ComputeArithmetic"){
        ComputeArithmetic CA;
        for (int i = 1; i < c+1; i++) {
            float res = CA.compute(i*freq);
            float lim = CA.getAnalyticPrediction(i*freq);
            os<<i*freq<<" ";
            cout<<setiosflags(ios::scientific)<<setprecision(p)<<res<<" "<<lim<<"\n";
            //stringstream sstr;
            //sstr << i*freq;
            //string fstr = sstr.str();
            //this ->prt +=fstr;
            //this ->prt += " ";
            //stringstream ss;
            //ss << res;
            //string rstr = ss.str();
            //this ->prt +=rstr+" ";
            //stringstream ls;
            //ls << lim;
            //string lstr = ls.str();
            //this ->prt += lstr+"\n";
        }
    }
    else{
        if (method == "ComputePi"){
            ComputePi CP;
            for (int i = 1; i < c+1; i++) {
                float res = CP.compute(i*freq);
                float lim = CP.getAnalyticPrediction(i*freq);
                os<<i*freq<<" ";
                cout<<setiosflags(ios::scientific)<<setprecision(p)<<res<<" "<<lim<<"\n";
                //stringstream sstr;
                //sstr << i*freq;
                //string fstr = sstr.str();
                //this ->prt +=fstr;
                //this ->prt += " ";
                //stringstream ss;
                //ss << res;
                //string rstr = ss.str();
                //this ->prt +=rstr+" ";
                //stringstream ls;
                //ls << lim;
                //string lstr = ls.str();
                //this ->prt += lstr+"\n";
            }
            cout << prt;
        }
        else{
            cout << "Wrong Method \n";
        }
    }
};
void WriteSeries::setSeparator(string input) {
    sp = input;
}
void WriteSeries::dump(ostream & os) {
    unsigned int m = maxiter;
    if(method == "ComputeArithmetic"){
        ComputeArithmetic CA;
        for (int i = 1; i < m; i++) {
            float res = CA.compute(i);
            float lim = CA.getAnalyticPrediction(i);
            stringstream sstr;
            sstr << i;
            string fstr = sstr.str();
            this ->wrt +=fstr;
            this ->wrt += " ";
            stringstream ss;
            ss << res;
            string rstr = ss.str();
            this ->wrt +=rstr+" ";
            stringstream ls;
            ls << lim;
            string lstr = ls.str();
            this ->wrt += lstr+"\n";
        }
    }
    else{
        if (method == "ComputePi"){
            ComputePi CP;
            for (int i = 1; i < m; i++) {
                float res = CP.compute(i);
                float lim = CP.getAnalyticPrediction(i);
                stringstream sstr;
                sstr << i;
                string fstr = sstr.str();
                this ->wrt +=fstr;
                this ->wrt += " ";
                stringstream ss;
                ss << res;
                string rstr = ss.str();
                this ->wrt +=rstr+" ";
                stringstream ls;
                ls << lim;
                string lstr = ls.str();
                this ->wrt += lstr+"\n";
            }
        }
        else{
            cout << "Wrong Method \n";
        }
    }
    if (sp==" "){
        ofstream outfile("WriteSeries.txt");
        outfile << wrt;
        outfile.close();
    }
    else{
        if (sp==","){
            ofstream outfile("WriteSeries.csv");
            outfile<<wrt;
            outfile.close();
        }
        else{
            if(sp=="|"){
                ofstream outfile("WriteSeries.psv");
                outfile<<wrt;
                outfile.close();
            }
            else{
                cout << "Wrong Separator"<<endl;
            }
        }
    }
}
void PrintSeries::setPrecision(unsigned int input){
    precision = input;
};
void WriteSeries::setPrecision(unsigned int input){
    precision = input;
};
