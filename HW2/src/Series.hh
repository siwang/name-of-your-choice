#include <utility>
#include <math.h>
//
// Created by siwang@INTRANET.EPFL.CH on 22.10.20.
//

#ifndef SERIES_SERIES_HH
#define SERIES_SERIES_HH
class Series{
public:
    std::basic_string<char> getmethod();
    void setmethod(std::string input);
    virtual double compute(unsigned int N) = 0;
    virtual double getAnalyticPrediction(unsigned int k){
        return nan("Not a Number");
    };
private:
    std::string method;
};
class ComputeArithmetic : public Series{
public:
    double compute(unsigned int N) override;
    double getAnalyticPrediction(unsigned int k)override;
private:
    std::string method;
};
class ComputePi : public Series{
public:
    double compute(unsigned int N) override;
    double getAnalyticPrediction(unsigned int k)override;
private:
    std::string method;
};
#endif //SERIES_SERIES_HH
