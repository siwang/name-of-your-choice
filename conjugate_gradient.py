#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  9 09:44:56 2020

@author: siwang
"""

import scipy.optimize
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import argparse

parser = argparse.ArgumentParser()
fig = plt.figure()
axe = Axes3D(fig)

#define the function S(x)
def S(x,A,b):
    p1 = 1/2*np.einsum('ik,k->i',A,x.T)
    Sx=np.einsum('k,k->',p1,x)-np.einsum('k,k->',x.T,b)
    return Sx

#define the callback function

x = []
y = []
W = []

def callbackF(xk):
   global x
   global y
   global W
   fill1=[0]
   fill2=[0]
   fill3=[0]
   fill1[0]=xk[0]
   fill2[0]=xk[1]
   fill3[0]=S2(xk)
   x = x+fill1
   y = y+fill2
   W = W+fill3


#using CG method to calculate the minimum value 
def minimum(fun,x):
    res = scipy.optimize.minimize(fun,x,callback=callbackF, method ='CG')
    return res

#Add two matrix in previous question as argument
parser.add_argument('--v1', type=float,default = np.array(((4,0),(1,3))),
                    help='First matrix in previous question')
parser.add_argument('--v2', type=float,default = np.array(((0),(1))),
                    help='Second matrix in previous question')

args = parser.parse_args()

#define a new S(x) function with matrix A and b from previous question
def S2(x):
    Sx = S(x,2*args.v1,args.v2)
    return Sx

#Calculate the minimum value
x0 = ([3],[3])
x0 = np.array(x0)
x0 = x0.flatten()
res = minimum(S2,x0)

#define the range of X and Y values
X = np.arange(-3, 3, 0.01)
Y = np.arange(-3, 3, 0.01)
Z = np.zeros((600,600))

#calculate Y values based on X and Y
for i in range(600):
    for j in range(600):
        M = ([X[i]],[Y[j]])
        M = np.array(M)
        M = M.flatten()
        Z[i,j]=S(M,2*args.v1, args.v2)
X, Y = np.meshgrid(X, Y)
#plot the surface and iterations       
axe.plot_surface(X,Y,Z,alpha = 0.5)
axe.plot(x,y,W,'o--')
axe.set_title("Conjugated Gradient")